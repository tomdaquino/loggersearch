#!/usr/bin/python3

import os
import re
import gzip
import shlex
import pandas as pd
from ftplib import FTP
from datetime import datetime, timedelta
import logging

ftp_server = ''
ftp_user = ''
ftp_password = ''

# Set base parameters:
#   days - type: int, required: yes, purpose: number of past days to include in search results
#   path_stub - type: string, required: yes, purpose: root path to the logger files before variable dir names
#   plugin_id - type: string, required: yes, purpose: the plugin_id to match
#   plugin_sid - type: list, required: yes, purpose: a list of plugin_sid values to match
#   filter_include - type: list, required: no, purpose: list of key='value' matches to include (any matching item in list will result in event being included in search results)
#   filter_exclude - type: list, required: no, purpose: list of key='value' matches to exclude (any matching item in list will result in event being excluded from search results)
#   filter_regex_exclude - type: string, required: no, purpose: exclude an event from search results based on a regex string
days = 31
path_stub = "/var/ossim/logs"
plugin_id = ""
plugin_sid = []
filter_include = []
filter_exclude = []
filter_regex_exclude = "username='.*\$'"


def stats_search(stats_file):
    # Check the given stats_file for the presence of a plugin_id, plugin_sid pair
    data_stats_file = open(stats_file)
    data_stats_text = data_stats_file.read()
    data_stats_file.close()
    for psid in plugin_sid:
        matches = re.findall('plugin_sid:' + plugin_id + ',' + psid + ':\d+', data_stats_text)
        if matches:
            return True
    return None


def build_id_list(id_list, id_type):
    # Convert plugin_id to list with plugin_id='plugin_id' format
    new_id_list = []
    for i in id_list:
        new_id_list.append(id_type + "='" + i + "'")
    return new_id_list


def clean_event(event):
    # Strip the "<entry" and "/>" values from the raw event
    strip = re.search('<entry (.*)/>', event)
    if strip:
        strip_event = strip.group(1)
        return strip_event
    else:
        return None


def split_event(event):
    # Split fields into a list of key='value' elements
    event_list = re.findall("\S+='[^']+'", event)
    return event_list


def create_event_dict(event):
    # Convert event from key='value' format to dict
    event_dict = dict(token.split('=', 1) for token in shlex.split(event))
    event_dict['raw_event'] = event
    return event_dict


def get_events(remote_filename):
    # Primary function that searches for events and includes/excludes events based on filter parameters
    events = []
    day_count = 0
    while day_count <= days:
        date_object = datetime.now() - timedelta(days=day_count)
        date_year = date_object.strftime('%Y')
        date_month = date_object.strftime('%m')
        date_day = date_object.strftime('%d')
        date_path = path_stub + '/' + date_year + '/' + date_month + '/' + date_day
        sub_dirs =  [f.path for f in os.scandir(date_path) if f.is_dir()]
        for sub_dir in sub_dirs:
            data_stats_file = sub_dir + '/data.stats'
            stats_match = stats_search(data_stats_file)
            if stats_match:
                ip_dirs = [f.path for f in os.scandir(sub_dir) if f.is_dir()]
                for ip_dir in ip_dirs:
                    for filename in os.listdir(ip_dir):
                        file_events = []
                        if re.match(".*\.log\.gz", filename):
                            f = gzip.open(ip_dir + '/' + filename, 'rt', encoding='iso-8859-1')
                            for line in f:
                                event = clean_event(line)
                                if event:
                                    sid_match = None
                                    include_match = None
                                    exclude_match = None
                                    fields = split_event(event)
                                    id_include = build_id_list([plugin_id], 'plugin_id')
                                    id_match = any(elem in fields for elem in id_include)
                                    if id_match:
                                        sid_include = build_id_list(plugin_sid, 'plugin_sid')
                                        sid_match = any(elem in fields for elem in sid_include)
                                    if id_match and sid_match:
                                        include_match = any(elem in fields for elem in filter_include)
                                        exclude_match = any(elem in fields for elem in filter_exclude)
                                    if id_match and sid_match and include_match and not exclude_match:
                                        regex_exclude_match = re.findall(filter_regex_exclude, event)
                                        if not regex_exclude_match:
                                            event_dict = create_event_dict(event)
                                            file_events.append(event_dict)
                            df = pd.DataFrame(file_events)
                            grouped = df.groupby(['plugin_sid', 'ctx', 'src_host', 'dst_host', 'username',
                                                  'userdata1', 'userdata2', 'userdata3', 'userdata5', 'userdata6',
                                                  'userdata7', 'userdata8', 'userdata9', 'device']).agg(sum)
                            aggr_events = grouped.reset_index().to_dict('records')
                            out_file = open(remote_filename, 'a+', encoding='iso-8859-1')
                            for aggr_event in aggr_events:
                                raw_event = aggr_event['raw_event']
                                out_file.write(raw_event + '\n')
        day_count += 1
    return events


def main():
    now = datetime.now()
    date_string = now.strftime("%d_%m_%Y")
    remote_filename = 'alienvault_logger_export_' + date_string + '.txt'
    get_events(remote_filename)
    upload_file = open(remote_filename, 'rb')
    ftp = FTP(ftp_server)
    ftp.login(ftp_user, ftp_password)
    ftp_command = 'STOR ' + remote_filename
    ftp.storbinary(ftp_command, upload_file)
    ftp.quit()
    upload_file.close()
    os.remove(remote_filename)


if __name__ == "__main__":
    logging.basicConfig(filename='/var/log/loggersearch.log', format='%(asctime)s %(message)s', level=logging.INFO)
    try:
        main()
    except Exception:
        logging.exception("Fatal error in main")