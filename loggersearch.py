#!/usr/bin/python3

import os
import re
import io
import gzip
from ftplib import FTP
from datetime import datetime, timedelta
import logging

ftp_server = ''
ftp_user = ''
ftp_password = ''

path_stub = "/var/ossim/logs"
plugin_id = ""
plugin_sid = []
filter_include = []
filter_exclude = []


def stats_search(stats_file):
    data_stats_file = open(stats_file)
    data_stats_text = data_stats_file.read()
    data_stats_file.close()
    for psid in plugin_sid:
        matches = re.findall('plugin_sid:' + plugin_id + ',' + psid + ':\d+', data_stats_text)
        if matches:
            return True
    return None


def build_id_list(id_list, id_type):
    new_id_list = []
    for i in id_list:
        new_id_list.append(id_type + "='" + i + "'")
    return new_id_list


def clean_event(event):
    strip = re.search('<entry (.*)/>', event)
    if strip:
        strip_event = strip.group(1)
        return strip_event
    else:
        return None


def split_event(event):
    event_list = re.findall("\S+='[^']+'", event)
    return event_list

def get_events():
    events = []
    day_count = 1
    while day_count <= 32:
        date_object = datetime.now() - timedelta(days=day_count)
        date_year = date_object.strftime('%Y')
        date_month = date_object.strftime('%m')
        date_day = date_object.strftime('%d')
        date_path = path_stub + '/' + date_year + '/' + date_month + '/' + date_day
        sub_dirs =  [f.path for f in os.scandir(date_path) if f.is_dir()]
        for sub_dir in sub_dirs:
            data_stats_file = sub_dir + '/data.stats'
            stats_match = stats_search(data_stats_file)
            if stats_match:
                ip_dirs = [f.path for f in os.scandir(sub_dir) if f.is_dir()]
                for ip_dir in ip_dirs:
                    for filename in os.listdir(ip_dir):
                        if re.match(".*\.log\.gz", filename):
                            f = gzip.open(ip_dir + '/' + filename, 'rt', encoding='iso-8859-1')
                            for line in f:
                                event = clean_event(line)
                                if event:
                                    sid_match = None
                                    include_match = None
                                    exclude_match = None
                                    fields = split_event(event)
                                    id_include = build_id_list([plugin_id], 'plugin_id')
                                    id_match = any(elem in fields for elem in id_include)
                                    if id_match:
                                        sid_include = build_id_list(plugin_sid, 'plugin_sid')
                                        sid_match = any(elem in fields for elem in sid_include)
                                    if id_match and sid_match:
                                        include_match = any(elem in fields for elem in filter_include)
                                        exclude_match = any(elem in fields for elem in filter_exclude)
                                    if id_match and sid_match and include_match and not exclude_match:
                                        events.append(event)
        day_count += 1
    return events


def main():
    now = datetime.now()
    date_string = now.strftime("%d_%m_%Y")
    remote_filename = 'alienvault_logger_export_' + date_string + '.txt'
    event_list = get_events()
    s = '\n'
    event_string = s.join(event_list)
    sio = io.StringIO(event_string)
    ftp = FTP(ftp_server)
    ftp.login(ftp_user, ftp_password)
    ftp_command = 'STOR ' + remote_filename
    ftp.storlines(ftp_command, sio)
    ftp.quit()


if __name__ == "__main__":
    logging.basicConfig(filename='/var/log/loggersearch.log', format='%(asctime)s %(message)s', level=logging.INFO)
    try:
        main()
    except Exception:
        logging.exception("Fatal error in main")